import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;
import java.util.Scanner;

public class HeartRates
{
   private String firstName;
   private String lastName;
   private int dayOfBirth;
   private int monthOfBirth;
   private int yearOfBirth;

    public HeartRates()
    {

    }

    public HeartRates(String firstName, String lastName, int dayOfBirth, int monthOfBirth, int yearOfBirth)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.monthOfBirth = monthOfBirth;
        this.dayOfBirth = dayOfBirth;
        this.yearOfBirth = yearOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getMonthOfBirth() { return monthOfBirth; }

    public void setMonthOfBirth(int monthOfBirth) { this.monthOfBirth = monthOfBirth; }

    public int getDayOfBirth() { return dayOfBirth; }

    public void setDayOfBirth(int dayOfBirth) { this.dayOfBirth = dayOfBirth; }

    public int getYearOfBirth() { return yearOfBirth; }

    public void setYearOfBirth(int yearOfBirth) {this.yearOfBirth = yearOfBirth; }

    //creates a string with the date of birth
    public String calculateDateOfBirth()
    {
        String dateOfBirth = this.dayOfBirth + " - " + this.monthOfBirth + " - " + this.yearOfBirth;
        return dateOfBirth;
    }

    public int calculateAge()
    {
        //subtracts the year of birth from the current year in order to calculate the person's age
        Calendar year =  new GregorianCalendar();
        int currentYear = year.get(Calendar.YEAR);
        int age = currentYear - this.yearOfBirth;
        return age;

    }

    public int calculateMaxHeartRate()
    {
        //maximum heart rate is 220 minus the age in years
        int maxHeartRate = 220 - calculateAge();
        return maxHeartRate;
    }

    public String calculateTargetHeartRate()
    {
        //target heart rate is a range that is 50-58% of the maximum heart rate
        int inferiorHeartRateLimit = calculateMaxHeartRate() * 50 / 100;
        int superiorHeartRateLimit = calculateMaxHeartRate() * 85 / 100;
        //creates a string with the above limits
        String targetHeartRateRange = inferiorHeartRateLimit + " - " + superiorHeartRateLimit;
        return targetHeartRateRange;

    }

    public static void getUserInformation(){

        //user inputs name, last name and date of birth.
        System.out.println("Enter your first name: ");
        Scanner scannerName = new Scanner(System.in);
        String name = scannerName.nextLine();

        System.out.println("Enter your last name: ");
        Scanner scannerLast = new Scanner(System.in);
        String lastName = scannerLast.nextLine();

        //user can type birth date like this (dd-mm-yyyy) or like this (dd/mm/yyyy)
        System.out.println("Enter your date of birth (dd-mm-yyyy): " );
        Scanner scanner = new Scanner(System.in);
        String dateOfBirth = scanner.nextLine();

        String []date = dateOfBirth.split("[-/]"); //splits either - or /
        int dayOfBirth = Integer.parseInt(date[0].trim());
        int monthOfBirth = Integer.parseInt(date[1].trim());
        int yearOfBirth = Integer.parseInt(date[2].trim());
        //calculates the current year
        Calendar year =  new GregorianCalendar();
        int currentYear = year.get(Calendar.YEAR);
        //checks if user inputs are valid and if not it throws the below exception
        if((dayOfBirth <= 31)&&(monthOfBirth <= 12)&&((yearOfBirth <= currentYear)&&(yearOfBirth >= 1900))){
            HeartRates person = new HeartRates(name, lastName, dayOfBirth, monthOfBirth, yearOfBirth);
            System.out.println(person.toString());
        }else {
            throw new InputMismatchException("Your date of birth is invalid");
        }
    }
    @Override
    public String toString() {
        return  "Name : " + firstName + "\n " +
                "Last Name : " + lastName + "\n " +
                "Date of Birth : " + calculateDateOfBirth() + "\n " +
                "Age : " + calculateAge() + "\n " +
                "Maximum Heart Rate : " + calculateMaxHeartRate() + "\n " +
                "Target Heart Rate range : " + calculateTargetHeartRate() + "\n " +
                "------------------------------------";
    }
}
